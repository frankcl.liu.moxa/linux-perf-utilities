# linux-perf-utilities


## Getting started

### Prerequisites

#### Install `linux-perf`

#### Install dependencies

```
$ sudo apt-get update
$ sudo apt-get install -y libbabeltrace-dev
```

### Usage

1. Clone this project:
```
$ git clone git@gitlab.com:frankcl.liu.moxa/linux-perf-utilities.git
```

2. Setup dependencies
```
$ ./setup_dependencies.sh
```

3. Run the program to be captured

4. Add probe events
```
$ ./add_probes.sh </path/to/executable/or/shared/library/file> [symbol filter]
```

5. Capture data
```
$ ./capture.sh <capture duration (seconds)> <program name or pid>
```

6. View the report

    Load the `output/trace.json` into `chrome://tracing` in Google Chrome

7. Delete probe events
```
$ ./delete_all_probes.sh
```

## References


## Contact Me
- Maintainer: Frank CL Liu - frankcl.liu@moxa.com
