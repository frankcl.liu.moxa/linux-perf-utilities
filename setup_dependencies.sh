# Install babel trace
echo "(1/3) update submodule..."
git submodule update --init --recursive

cd ctf2ctf

echo "(2/3) apply patch..."
git apply ../modify-suffix-string-matching-for-function-probe.patch

echo "(3/3) build ctf2ctf..."
mkdir -p ./build
cmake -B ./build -S ./
make -j$(nproc) -C ./build

cd ..

echo "setup dependencies done"
