
# Usage: ./add_probes.sh </path/to/executable/or/shared/library/file> [symbol filter]

dsymbols=$(nm --dynamic --demangle --defined-only $1 | grep -E "$2" | awk '$2=="t" || $2=="T"' | awk '$3~/^[_A-Za-z][_0-9A-Za-z]*$/' | cut -d ' ' -f 1,3)
nsymbols=$(nm           --demangle --defined-only $1 | grep -E "$2" | awk '$2=="t" || $2=="T"' | awk '$3~/^[_A-Za-z][_0-9A-Za-z]*$/' | cut -d ' ' -f 1,3)

symbols="$dsymbols"$'\n'"$nsymbols"

echo "$symbols" | while read -r addr symbol; do

    if [ -n "$addr" ] && [ -n "$symbol" ]; then

        module_name=$(basename $1)
        module_name=${module_name//-/_}
        module_name=${module_name%%.*}
     
        function_name=$(echo "$symbol" | sed 's/[(<].*//')
        function_name="${function_name##*::}"

        address=0x$addr

        echo -e "Setting Probes for $symbol: $module_name::$function_name ($address)"

        # Set entry probe
        perf probe -x $1 -f -a probe_${module_name}:${function_name}__entry=${symbol}

        # Set return probe
        perf probe -x $1 -f -a probe_${module_name}:${function_name}__return=${symbol}%return

    fi

done
