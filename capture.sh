
# Usage: ./capture.sh <capture duration (seconds)> <process name>

capture_duration=$1

target_pid=$2
if  [ -z "${target_pid##*[!0-9]*}" ]; then
    target_pid=$(pgrep -o $2)
    echo "Target process $2 with pid $target_pid"
fi

echo "Capturing pid $target_pid for $capture_duration seconds"

OUTPUT_DIR=./output
mkdir -p $OUTPUT_DIR
rm -rf $OUTPUT_DIR/*

perf record --pid=$target_pid --no-buildid --realtime=50 --all-cpus -e probe_*:* -o $OUTPUT_DIR/trace.data sleep $capture_duration
echo "perf record completed"

perf data convert -i $OUTPUT_DIR/trace.data --to-ctf $OUTPUT_DIR/trace_data
echo "CTF conversion completed"

./ctf2ctf/build/ctf2ctf $OUTPUT_DIR/trace_data/ --pid-whitelist=$target_pid >  $OUTPUT_DIR/trace.json
echo "JSON conversion completed"
